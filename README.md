# Deploy runners

This repo aims at helping registering and running gitlab-ci-runners. It will 
use current hostname and user as tag for the registred runner and only run it
on this machine.

# Fresh start

Before being run, runners has to be registered. This have to be done once. 
In order to do that, a registration token specific to the gitlab's
project. It can be found in the "Settings > CI/CD" section in the gitlab web
interface (**important note: you have to be Master or Owner of the project to
access this information**).

To do so use the "register" script. It takes the token as argument. As default
it tags the registred runner as the current hostname and machine and uses the 
gitlab url "dci-gitlab.cines.fr". Then use the "run" script to launch the runners by invoking the hangups immune **nohup** utility. It takes the project name as arguments.

Example:
```shell
#You will need a python, if not in PATH
module load cray-python
python3 ./register <TOKEN> <NOM_DU_RUNNER> --site-url <URL> 
nohup python3 ./run <NOM_DU_RUNNER> >& <NOM_DU_RUNNER>.log &
```
**Reminder:** Do not forget to tag this new runner with the keyword that corresponds to the default tag of the project where you want to use this runner

# One runner, multiple workers

From [gitlab documentation : runner](https://docs.gitlab.com/runner/fleet_scaling/index.html#intermediate-configuration-one-runner-multiple-workers) : You can also register multiple runner workers on the same machine. When you do this, the runner’s config.toml file has multiple [[runners]] sections in it. If all of the additional runner workers are registered to use the shell executor, and you update the value of the global configuration option, concurrent, to 3, the upper limit of jobs that can run concurrently on this host is equal to three.

```shell
concurrent = 3

[[runners]]
  name = "instance_level_shell_001"
  url = ""
  token = ""
  executor = "shell"

[[runners]]
  name = "instance_level_shell_002"
  url = ""
  token = ""
  executor = "shell"

[[runners]]
  name = "instance_level_shell_003"
  url = ""
  token = ""
  executor = "shell"
  ```
You can register many runner workers on the same machine, and each one is an isolated process. The performance of the CI/CD jobs for each worker is dependent on the compute capacity of the host system.
